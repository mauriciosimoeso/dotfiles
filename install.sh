#!/bin/bash

echo "Installing starship..."
curl -sS https://starship.rs/install.sh | sh
cp ~/.zshrc ~/.zshrc.bkp
echo "eval \"\$(starship init zsh)\"" >> ~/.zshrc

# TODO should check if the folder already exists
echo "Downloading wallpapers..."
git clone https://github.com/Asthetic/AnimeBackgrounds.git ~/wallpapers
# TODO should configure wallpapers on KDE/Gnome/etc

# TODO same check
echo "Installing tmux plugin manager..."
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# TODO should check for stow
echo "Symlinking configuration..."
stow .
